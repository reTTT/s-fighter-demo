extends "res://script/game/state.gd"

var player
var target
var ai

var waitTime = 0

func state_enter(owner):
	player = owner
	ai = player.get_node("ai")
	target = player.get_target()

	owner.set_state(owner.S_IDLE)


func state_execute(owner):
	if ai.we_need_be_protected():
		get_parent().change_state("blocking")
		return
	elif ai.is_can_attack():
		ai.attack()
		return

	if waitTime > 0:
		return

	var dist = ai.get_dist_to_target()

	if dist > ai.max_dist:
		get_parent().change_state("walk_up")
	elif dist > (ai.min_dist + ai.max_dist)/2:
		if rand_range(0, 100) < 75:
			waitTime = rand_range(.5, 1.0)
			set_process(true)
		elif rand_range(0, 100) < 50:
			get_parent().change_state("move_away")
		else:
			get_parent().change_state("walk_up")
	else:
		get_parent().change_state("move_away")


func state_exit(owner):
	set_process(false)


func _process(dt):
	waitTime -= dt
	if waitTime <= 0:
		set_process(false)
	
