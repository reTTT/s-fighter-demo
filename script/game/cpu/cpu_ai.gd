extends Node

var player
var target

var min_dist = 40
var max_dist = 64

func get_dist_to_target():
	var delta = target.get_pos() - player.get_pos()
	return delta.length()


func is_can_attack():
	if player.get_state() == player.S_HIT:
		return false
	
	if target.get_state() == target.S_HIT:
		return false
	
	if get_dist_to_target() > min_dist:
		return false
 
	return rand_range(0, 100) < 35


func we_need_be_protected():
	var dist = get_dist_to_target()
	
	if dist > min_dist:
		return false
	
	if target.get_state() != player.S_KICK && target.get_state() != player.S_PUNCH:
		return false
	
	return rand_range(0, 100) < 25


func attack():
	if rand_range(0, 100) > 50:
		get_node("fsm").change_state("kick")
	else:
		get_node("fsm").change_state("punch")


func _process(dt):
	if player.get_state() == player.S_HIT && get_node("fsm").get_name_of_current_state() != "hit":
		get_node("fsm").change_state("hit")


func _ready():
	player = get_node("..")
	target = player.get_target()
	set_process(true)
