extends "res://script/game/state.gd"

var player
var target
var ai

func state_enter(owner):
	player = owner
	ai = player.get_node("ai")
	target = player.get_target()

	owner.set_state(owner.S_BLOCKING)


func state_execute(owner):
	if target.get_state() != player.S_KICK && target.get_state() != player.S_PUNCH:
		if ai.is_can_attack():
			ai.attack()
		else:
			get_parent().change_state("idle")

func state_exit(owner):
	pass