extends "res://script/game/state.gd"


var player
var target
var ai

func state_enter(owner):
	player = owner
	ai = player.get_node("ai")
	target = player.get_target()


func state_execute(owner):
	var delta = target.get_pos() - player.get_pos()
	
	if delta.length() < ai.max_dist:
		player.walking_backward()
	else:
		get_parent().change_state("idle")


func state_exit(owner):
	pass