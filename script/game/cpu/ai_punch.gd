extends "res://script/game/state.gd"

var player
var target
var ai

func state_enter(owner):
	player = owner
	ai = player.get_node("ai")
	target = player.get_target()

	owner.set_state(owner.S_PUNCH)


func state_execute(owner):
	if player.get_state() != player.S_PUNCH:
		if ai.is_can_attack()  && rand_range(0, 100) < 20:
			ai.attack()
		else:
			get_parent().change_state("move_away")


func state_exit(owner):
	pass