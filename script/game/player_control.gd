extends Node

var player = null

var state = "idle"
var next_state = ""

var state_fn = {
	"walking_left":funcref(self, "walking_left"),
	"walking_right":funcref(self, "walking_right"),
	"blocking":funcref(self, "blocking"),
	"kick":funcref(self, "kick"),
	"punch":funcref(self, "punch"),
	"idle":funcref(self, "idle")
}

func _ready():
	player = get_node("..")
	set_process_input(true)
	set_process(true)
	player.set_state(player.S_IDLE)


func walking_left(dt):
	player.walking(-1)


func walking_right(dt):
	player.walking(1)


func blocking(dt):
	if player.get_state() != player.S_BLOCKING:
		player.set_state(player.S_BLOCKING)


func kick(dt):
	if player.get_state() == player.S_IDLE:
		next_state = "idle"


func punch(dt):
	if player.get_state() == player.S_IDLE:
		next_state = "idle"


func idle(dt):
	if player.get_state() != player.S_IDLE:
		player.set_state(player.S_IDLE)


func hit(dt):
	if player.get_state() == player.S_IDLE:
		next_state = "idle"


func is_attacking():
	return player.get_state() == player.S_KICK || player.get_state() == player.S_PUNCH


func _input(event):
	if player.get_state() == player.S_HIT:
		return

	if event.is_pressed():
		next_state = ""

		if event.is_action("kick") && !event.is_echo() && !is_attacking():
			next_state = "kick"
		elif event.is_action("punch") && !event.is_echo()  && !is_attacking():
			next_state = "punch"
		elif event.is_action("block") && !is_attacking():
			next_state = "blocking"
		
		if next_state.empty():
			if event.is_action("left"):
				next_state = "walking_left"
			elif event.is_action("right"):
				next_state = "walking_right"
	else:
		if is_attacking():
			return
	
		if event.is_action("left"):
			next_state = "idle"
		elif event.is_action("right"):
			next_state = "idle"
		elif event.is_action("block"):
			next_state = "idle"


func set_state(s):
	if s.empty() || state == s:
		return
	
	state = s
	print("state: " + s)
	
	if s == "kick":
		player.set_state(player.S_KICK)
	elif s == "punch":
		player.set_state(player.S_PUNCH)


func _process(delta):
	if player.get_state() == player.S_HIT:
		next_state = "hit"

	set_state(next_state)
	call(state, delta)