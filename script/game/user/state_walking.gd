extends "res://script/game/state.gd"

var player

func state_enter(owner):
	owner.get_node("spr/ani").play("walking")
	set_fixed_process(true)
	player = owner

func state_execute(owner):
	pass


func state_exit(owner):
	set_fixed_process(false)


func _fixed_process(delta):
	var move = player.get_move_dir() * delta * player.walking_speed
	var pos = player.get_pos()
	pos.x += move
	player.set_pos(pos)
