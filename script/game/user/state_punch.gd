extends "res://script/game/state.gd"

export var damage = 10
export var dist = 40
var player


func on_ani_finish():
	player.set_state(player.S_IDLE)


func on_punch():
	var target = player.get_target()
	var delta = target.get_pos() - player.get_pos()

	if delta.length() <= dist:
		target.add_damage(damage)


func state_enter(owner):
	player = owner
	var ani = owner.get_node("spr/ani")
	ani.play("punch")
	ani.connect("finished", self, "on_ani_finish")
	owner.get_node("spr").connect("on_punch", self, "on_punch")


func state_execute(owner):
	pass


func state_exit(owner):
	var ani = owner.get_node("spr/ani")
	ani.disconnect("finished", self, "on_ani_finish")
	owner.get_node("spr").disconnect("on_punch", self, "on_punch")
