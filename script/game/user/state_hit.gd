extends "res://script/game/state.gd"

var player

func on_ani_finish():
	player.set_state(player.S_IDLE)


func state_enter(owner):
	player = owner
	var ani = owner.get_node("spr/ani")
	ani.play("hit")
	ani.connect("finished", self, "on_ani_finish")


func state_execute(owner):
	pass


func state_exit(owner):
	var ani = owner.get_node("spr/ani")
	ani.disconnect("finished", self, "on_ani_finish")
