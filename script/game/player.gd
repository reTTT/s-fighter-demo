extends Node2D

const S_BLOCKING = 0
const S_HIT = 1
const S_IDLE = 2
const S_KICK = 3
const S_PUNCH = 4
const S_WALKING = 5

const T_RED = 0
const T_BLUE = 0

export var team = T_RED

export var health = 100
var damage = 0
var state = S_IDLE
var walking_speed = 50
var orientation = 0
var move_dir = 0

var table = {S_BLOCKING:"block", S_HIT:"hit", S_IDLE:"idle", S_KICK:"kick", S_PUNCH:"punch", S_WALKING:"walking"}

func _init():
	add_user_signal("change_state", ["state"]) 
	add_user_signal("change_health", ["health"])


func _ready():
	get_node("..").set_team(team, self)
	set_fixed_process(true)


func blocking():
	set_state(S_BLOCKING)


func kick():
	set_state(S_KICK)


func punch():
	set_state(S_PUNCH)


func get_orientation():
	return orientation


func walking(dir):
	self.move_dir = dir
	set_state(S_WALKING)


func walking_forward():
	walking(orientation)


func walking_backward():
	walking(orientation * -1)


func get_move_dir():
	return move_dir


func set_state(state):
	if self.state != state:
		self.state = state
		get_node("fsm").change_state(table[state])
		emit_signal("change_state", state)


func get_state():
	return state


func _fixed_process(delta):
	var target = get_target()
	if target:
		var tpos = target.get_pos()
		var cpos = get_pos()
		var dir = tpos - cpos
		set_orientation(sign(dir.x))


func get_target():
	return get_node("..").get_target(team)


func set_orientation(orientation):
	self.orientation = orientation
	get_node("spr").set_flip_h(orientation < 0)


func add_damage(d):
	if state == S_BLOCKING:
		d *= 0.25

	damage += d
	if damage >= health:
		damage = health
	
	emit_signal("change_health", get_health())

	if get_health():
		if state != S_BLOCKING:
			set_state(S_HIT)
	else:
		get_node("spr"). set_modulate(Color(0, 1, 0, 1))
	
	

func get_health():
	return health - damage