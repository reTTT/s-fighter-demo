extends ProgressBar

export(int) var tiam_id = 0

func on_change_health(health):
	 set_val(health)


func _ready():
	var player = get_node("/root/game/world").get_player(tiam_id)
	player.connect("change_health", self, "on_change_health")
	set_val(player.get_health())