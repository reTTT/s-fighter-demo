extends Node2D

func _init():
	add_user_signal("on_kick")
	add_user_signal("on_punch") 


func _on_kick():
	emit_signal("on_kick")


func _on_punch():
	emit_signal("on_punch")