extends Node

var team = {}

func _ready():
	pass


func set_team(team_id, player):
	team[team_id] = player


func get_player(team_id):
	if !team.has(team_id):
		return null

	return team[team_id]


func get_target(team_id):
	var t = team_id + 1
	if t > 1:
		t = 0

	if !team.has(t):
		return null

	return team[t]